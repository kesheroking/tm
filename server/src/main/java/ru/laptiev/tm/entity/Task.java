package ru.laptiev.tm.entity;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.Status;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_task")
public final class Task extends AbstractSecondEntity implements Serializable {

    @NotNull
    @Column(name = "project_id",
            nullable = false
    )
    private String projectID;

    @Nullable
    @Column(name = "description", insertable = false, updatable = false)
    private String taskDescription;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "project_id", insertable = false, updatable = false)

    private Project motherProject;

    public Task(@NotNull final String taskName, @NotNull final String projectID, @NotNull final String userID, @NotNull Status status) {
        super(UUID.randomUUID().toString(), taskName, userID, status, LocalDateTime.now(), null, null);
        this.projectID = projectID;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;
        return id != null && id.equals(((Task) o).getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
