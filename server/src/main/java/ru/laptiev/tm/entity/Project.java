package ru.laptiev.tm.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_project")
public final class Project extends AbstractSecondEntity implements Serializable {

    @Nullable
    @Column(name = "description", insertable = false, updatable = false)
    private String projectDescription;

    @OneToMany(mappedBy = "motherProject", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
    public List<Task> tasks = new ArrayList<>();

    public Project(@NotNull final String projectName, @NotNull final String userID, @NotNull Status status, @Nullable LocalDateTime dateBegin, @Nullable LocalDateTime dateEnd) {
        super(UUID.randomUUID().toString(), projectName, userID, status, LocalDateTime.now(), dateBegin, dateEnd);


    }

    public void addTask(Task task) {
        tasks.add(task);
        task.setMotherProject(this);
    }

    public void removeTask(Task task) {
        tasks.remove(task);
        task.setMotherProject(null);
    }
}

