package ru.laptiev.tm.entity;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.AccessRole;
import ru.laptiev.tm.MD5Hash;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="app_user")
@AttributeOverride(name = "name", column = @Column(name = "login", unique = true, length = 20))
public final class User extends AbstractEntity implements Serializable {

    @NotNull
    @Column(name="password_hash")
    private String password;

    @NotNull
    @Column(name="role")
    @Enumerated(EnumType.STRING)
    private AccessRole accessRole;

    public User(String name, String password, AccessRole role) {
        super(UUID.randomUUID().toString(), name, null, LocalDateTime.now(), null);
        this.password = MD5Hash.getMD5Hash(password);
        this.accessRole = role;
    }


}
