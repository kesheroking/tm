package ru.laptiev.tm.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.Status;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Project.class, Task.class})
@JsonAutoDetect
@MappedSuperclass
public abstract class AbstractSecondEntity extends AbstractEntity {
    @Nullable
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    @Column(name="date_begin")
    protected LocalDateTime dateBegin;

    @Nullable
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    @Column(name="date_end", insertable = false, updatable = false)
    protected LocalDateTime dateEnd;
    @Nullable
    @Column(name = "user_id")
    protected String userId;
    public AbstractSecondEntity(@NotNull String id, @NotNull final String name, @NotNull final String userID, @NotNull Status status,@NotNull LocalDateTime dateOfCreation, @Nullable LocalDateTime dateBegin,@Nullable LocalDateTime dateEnd){
        super(id,  name, status, dateOfCreation, null);
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.userId = userID;
    }
}
