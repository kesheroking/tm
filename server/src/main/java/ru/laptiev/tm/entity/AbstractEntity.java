package ru.laptiev.tm.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.Status;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ AbstractSecondEntity.class, User.class})
@JsonAutoDetect
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    @Nullable
    @Id
    @Column(name="id",insertable = false, updatable = false)
    protected String id;

    @NotNull
    @Column(name="name")
    protected String name;

    @Nullable
@Transient
    protected Status status;

    @NotNull
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    @Transient
    protected LocalDateTime dateOfCreation;


   @Nullable
   @Transient
    protected String description;

}