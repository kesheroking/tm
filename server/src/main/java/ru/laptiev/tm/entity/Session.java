package ru.laptiev.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Session extends AbstractEntity {
    private Long timestamp;
    private String userID;
    private String signature;
}

