package ru.laptiev.tm.util;

import java.sql.Connection;
import java.sql.DriverManager;

public final class UtilityJDBCConnectionClass {
    private  UtilityJDBCConnectionClass(){}
    public static Connection connect(Connection conn){
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "tm";
        String userName = "root";
        String password = "Qwer1234";

        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + dbName + "?allowPublicKeyRetrieval=true&useSSL=false", userName, password);
            if (conn != null)
            { System.out.println("Приложение подключилось к БД !");
return conn;}
            else
                System.out.println("Приложение НЕ подключилось к БД ?");

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            return conn;
        }
    }
}
