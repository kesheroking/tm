package ru.laptiev.tm.util;

import ru.laptiev.tm.dto.ProjectDTO;
import ru.laptiev.tm.dto.TaskDTO;
import ru.laptiev.tm.dto.UserDTO;
import ru.laptiev.tm.entity.Project;
import ru.laptiev.tm.entity.Task;
import ru.laptiev.tm.entity.User;

public class EntityToDTOConverter {
    public ProjectDTO convertToProjectDTO(Project project){
        return new ProjectDTO(
                project.getId(),
                project.getName(),
                project.getUserId(),
                project.getStatus(),
                project.getDescription(),
                project.getDateBegin(),
                project.getDateEnd()
        );

    }
    public TaskDTO convertToTaskDTO(Task task){
        return new TaskDTO(
                task.getId(),
                task.getName(),
                task.getProjectID(),
                task.getUserId(),
                task.getStatus());
    }
    public UserDTO convertToUserDTO(User user){
        return new UserDTO(
                user.getId(),
                user.getName(),
                user.getPassword(),
                user.getAccessRole());
    }
    public Project convertToProject(ProjectDTO projectDTO){
        return new Project(
                projectDTO.getId(),
                projectDTO.getUser_id(),
                projectDTO.getStatus(),
                projectDTO.getDateBegin(),
                projectDTO.getDateEnd()
        );
    }
    public Task convertToTask(TaskDTO taskDTO){
        return new Task(
                taskDTO.getName(),
                taskDTO.getProject_id(), taskDTO.getUser_id(), taskDTO.getStatus());
    }
}
