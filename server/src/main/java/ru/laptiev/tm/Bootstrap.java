package ru.laptiev.tm;

import lombok.Getter;
import lombok.Setter;
import ru.laptiev.tm.entity.Session;
import ru.laptiev.tm.repository.ProjectRepository;
import ru.laptiev.tm.repository.TaskRepository;
import ru.laptiev.tm.repository.UserRepository;
import ru.laptiev.tm.service.ProjectService;
import ru.laptiev.tm.service.SessionService;
import ru.laptiev.tm.service.TaskService;
import ru.laptiev.tm.service.UserService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Getter
@Setter
public class Bootstrap implements ServiceLocator {
    private ProjectRepository projectRepository;
    private ProjectService projectService;
    private TaskRepository taskRepository;
    private TaskService taskService;
    private UserRepository userRepository;
    private UserService userService;
    private SessionService sessionService;
    private Session currentSession;
    private String currentStamp;
    String driver = "com.mysql.jdbc.Driver";
    String url = "jdbc:mysql://localhost:3306/";
    String dbName = "tm";
    String userName = "root";
    String password = "Qwer1234";


    private void createDefaultUsers() {
        userService.registration("test", "test", "admin");
        userService.registration("test1", "test1", "user");
    }


    public void init() throws Exception {
        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);
        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);
        userRepository = new UserRepository();
        userService = new UserService(userRepository);
        sessionService = new SessionService(userRepository);

        createDefaultUsers();
    }
}
