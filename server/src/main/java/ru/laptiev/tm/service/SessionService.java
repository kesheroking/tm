package ru.laptiev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.AccessRole;
import ru.laptiev.tm.SignatureUtil;
import ru.laptiev.tm.entity.Session;
import ru.laptiev.tm.entity.User;
import ru.laptiev.tm.repository.IUserRepository;


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "SessionService")
public class SessionService {
    @NotNull
    private IUserRepository iUserRepository;

    @WebMethod(exclude = true)
    public IUserRepository getiUserRepository() {
        return iUserRepository;
    }
    @WebMethod(exclude = true)
    public void setiUserRepository(IUserRepository iUserRepository) {
        this.iUserRepository = iUserRepository;
    }

    public SessionService(@NotNull final IUserRepository iUserRepository) {
        this.iUserRepository = iUserRepository;
    }

    @WebMethod
    public void closeSession() {
    }

    @WebMethod
    public void closeSessionAll() {
    }

    @WebMethod
    public void getListSession() {
    }

    @WebMethod
    public void getUser() {
    }

    @WebMethod
    public Session openSession(@WebParam(name = "login") final @NotNull String login,
                               @WebParam(name = "password") final @NotNull String password) {
        User user = iUserRepository.findByName(login);
        Session session = new Session(10000000L, user.getId(), SignatureUtil.sign(user, "123", 100));
        return session;
    }

    @WebMethod
    public AccessRole showAccessRole(@WebParam(name = "session") final @NotNull Session session) {
        return iUserRepository.findOne(session.getUserID()).getAccessRole();
    }
}
