package ru.laptiev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.HibernateException;
import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.Status;
import ru.laptiev.tm.dto.TaskDTO;
import ru.laptiev.tm.entity.Project;
import ru.laptiev.tm.entity.Task;
import ru.laptiev.tm.repository.ITaskRepository;
import ru.laptiev.tm.util.EntityToDTOConverter;
import ru.laptiev.tm.util.UtilityHibernateConnection;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@WebService(name = "TaskService")
public final class TaskService implements Serializable {
    @NotNull
    private ITaskRepository iTaskRepository;
    @PersistenceContext(unitName = "tm")
    EntityManager em;

    @NotNull
    private EntityToDTOConverter entityToDTOConverter = new EntityToDTOConverter();


    @WebMethod(exclude = true)
    public ITaskRepository getITaskRepository() {
        return iTaskRepository;
    }

    @WebMethod(exclude = true)
    public void setITaskRepository(ITaskRepository iTaskRepository) {
        this.iTaskRepository = iTaskRepository;
    }

    @WebMethod(exclude = true)
    public EntityManager getEm() {
        return em;
    }

    @WebMethod(exclude = true)
    public void setEm(EntityManager em) {
        this.em = em;
    }

    @WebMethod
    public Object getTaskRepository() {
        return iTaskRepository;
    }

    @WebMethod
    public void setTaskRepository(
            @WebParam(name = "taskRepository") final @NotNull Object iTaskRepository) {
        this.iTaskRepository = (ITaskRepository) iTaskRepository;
    }

    public TaskService(@NotNull ITaskRepository iTaskRepository) {
        this.iTaskRepository = iTaskRepository;
    }

    @WebMethod
    public void newTask(@WebParam(name = "projectID") @NotNull final String projectID, @WebParam(name = "taskName") @NotNull final String taskName, @WebParam(name = "userID") @NotNull final String userID) {
        em = UtilityHibernateConnection.connectHibernate();
        try {
            em.getTransaction().begin();
            iTaskRepository.persist(new Task(taskName, projectID, userID, Status.PLANNED));
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @WebMethod
    public List<TaskDTO> showTasks(@WebParam(name = "projectID") @NotNull final String projectID) {
        em = UtilityHibernateConnection.connectHibernate();
        List<TaskDTO> taskDTOs = new ArrayList<>();
        try {
            em.getTransaction().begin();
            for (Task task : iTaskRepository.getListTaskByProjectId(projectID)) {
                taskDTOs.add(entityToDTOConverter.convertToTaskDTO(task));
            }
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return taskDTOs;

    }

    @WebMethod
    public void changeTask(@WebParam(name = "taskName") @NotNull final String taskName, @WebParam(name = "taskNewName") @NotNull final String taskNewName) {
        em = UtilityHibernateConnection.connectHibernate();
        try {
            em.getTransaction().begin();
            Task task = iTaskRepository.findByName(taskName);
            task.setName(taskNewName);
            iTaskRepository.merge(task);
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @WebMethod
    public void deleteTask(@WebParam(name = "taskName") @NotNull final String taskName) {
        em = UtilityHibernateConnection.connectHibernate();
        try {
            em.getTransaction().begin();
            iTaskRepository.remove(iTaskRepository.findByName(taskName).getId());
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @WebMethod
    public void deleteByProjectID(@WebParam(name = "projectID") @NotNull final String projectID) {
        em = UtilityHibernateConnection.connectHibernate();
        try {
            em.getTransaction().begin();
            iTaskRepository.removeByProjectID(projectID);
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @WebMethod
    public List<String> sortTasksByDateOfCreation(@WebParam(name = "userID") @NotNull final String userID) {
        List<String> tasks = new ArrayList<>();
        for (Task task : iTaskRepository.sortByDateOfCreation(userID)) {
            tasks.add(task.getName());
        }
        return tasks;
    }


    @WebMethod
    public List<String> sortTasksByStatus(@NotNull final String userID) {
        List<String> tasks = new ArrayList<>();
        for (Task task : iTaskRepository.sortByStatus(userID)) {
            tasks.add(task.getName());
        }
        return tasks;
    }

    @WebMethod
    public List<String> globalTaskSearch(@WebParam(name = "text") @NotNull final String text) {
        List<String> tasks = new ArrayList<>();
        for (Task task : iTaskRepository.globalSearch(text)) {
            tasks.add(task.getName());
        }
        return tasks;
    }

    @WebMethod
    public List<String> globalTaskSearchByDescription(@WebParam(name = "text") @NotNull final String text) {
        List<String> tasks = new ArrayList<>();
        for (Task task : iTaskRepository.globalSearchByTaskDescription(text)) {
            tasks.add(task.getName() + " ____ " + task.getTaskDescription());
        }
        return tasks;
    }


    @WebMethod
    public void simpleTaskSave() throws IOException {
        FileOutputStream fos = new FileOutputStream("./target/saveta.bin");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        ITaskRepository taskRepository = (ITaskRepository) this.getITaskRepository();
        oos.writeObject(taskRepository);
        oos.flush();
        oos.close();
    }

    @WebMethod
    public void simpleTaskLoad() throws IOException, ClassNotFoundException {
        FileInputStream fos = new FileInputStream("./target/saveta.bin");

        ObjectInputStream oin = new ObjectInputStream(fos);

        this.setITaskRepository((ITaskRepository) oin.readObject());

    }

    @WebMethod
    public void jacksonJSONTaskSave() throws IOException {

        ITaskRepository taskRepository = ((ITaskRepository) this.getITaskRepository());
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        objectMapper.writeValue(new File("./target/jacksonjsontasks.json"), taskRepository);

    }

    @WebMethod
    public void jacksonJSONTaskLoad() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        this.setITaskRepository((ITaskRepository) objectMapper.readValue(new File("./target/jacksonjsontasks.json"), ITaskRepository.class));
    }

    @WebMethod
    public void jacksonXMLTaskSave() throws IOException {
        ITaskRepository taskRepository = ((ITaskRepository) this.getITaskRepository());
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.findAndRegisterModules();
        xmlMapper.writeValue(new File("./target/jacksonXMLtasks.xml"), taskRepository);
    }

    @WebMethod
    public void jacksonXMLTaskLoad() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.findAndRegisterModules();
        File file = new File("./target/jacksonXMLtasks.xml");
        this.setITaskRepository((ITaskRepository) xmlMapper.readValue(new File(file.getPath()), ITaskRepository.class));
    }

    @WebMethod
    public void jaxBJSONTaskSave() throws IOException, JAXBException {

        ITaskRepository taskRepository = ((ITaskRepository) this.getITaskRepository());

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Map<String, Object> properties = new HashMap<String, Object>(1);
        properties.put("eclipselink.media-type", "application/json");
        JAXBContext jc2 = (JAXBContext) JAXBContext.newInstance(new Class[]{ITaskRepository.class}, properties);
        Marshaller marshaller2 = jc2.createMarshaller();
        marshaller2.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
        marshaller2.marshal(taskRepository, new File("./target/tmptaskdataJSON.json"));
    }

    @WebMethod
    public void jaxBJSONTaskLoad() throws IOException, JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Map<String, Object> properties = new HashMap<String, Object>(1);
        properties.put("eclipselink.media-type", "application/json");
        JAXBContext jc2 = (JAXBContext) JAXBContext.newInstance(new Class[]{ITaskRepository.class}, properties);
        Unmarshaller unmarshaller2 = jc2.createUnmarshaller();
        this.setITaskRepository((ITaskRepository) unmarshaller2.unmarshal(new File("./target/tmptaskdataJSON.json")));
    }

    @WebMethod
    public void jaxBXMLTaskSave() throws IOException, JAXBException {
        ITaskRepository taskRepository = ((ITaskRepository) this.getITaskRepository());
        javax.xml.bind.JAXBContext context2 = JAXBContext.newInstance(ITaskRepository.class);
        Marshaller mar2 = context2.createMarshaller();
        mar2.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar2.marshal(taskRepository, new File("./target/tmptaskdata.xml"));
    }

    @WebMethod
    public void jaxBXMLTaskLoad() throws IOException, JAXBException {
        JAXBContext context2 = (JAXBContext) JAXBContext.newInstance(ITaskRepository.class);
        this.setITaskRepository((ITaskRepository) context2.createUnmarshaller().unmarshal(new FileReader("./target/tmptaskdata.xml")));
        //    @WebMethod
//    public List<String> sortTasksByStartDate(
//            @WebParam(name = "userID") @NotNull final String userID) {
//        List<String> tasks = new ArrayList<>();
//        for (Task task :
//                taskRepository.sortByStartDate(userID)) {
//            tasks.add(task.getName());
//        }
//        return tasks;
//    }
//
//    @WebMethod
//    public List<String> sortTasksByFinishDate(
//            @WebParam(name = "userID") @NotNull final String userID) {
//        List<String> tasks = new ArrayList<>();
//        for (Task task :
//                taskRepository.sortByFinishDate(userID)) {
//            tasks.add(task.getName());
//        }
//        return tasks;
//    }

    }
}
