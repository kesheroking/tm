package ru.laptiev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.HibernateException;
import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.Status;
import ru.laptiev.tm.dto.ProjectDTO;
import ru.laptiev.tm.entity.Project;
import ru.laptiev.tm.entity.Task;
import ru.laptiev.tm.repository.IProjectRepository;
import ru.laptiev.tm.repository.ProjectRepository;
import ru.laptiev.tm.util.EntityToDTOConverter;
import ru.laptiev.tm.util.UtilityHibernateConnection;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@Getter
//@Setter
@WebService(name = "ProjectService")
public final class ProjectService implements Serializable {


    @WebMethod(exclude = true)
    public IProjectRepository getIProjectRepository() {
        return iProjectRepository;
    }

    @WebMethod(exclude = true)
    public void setIProjectRepository(IProjectRepository iProjectRepository) {
        this.iProjectRepository = iProjectRepository;
    }

    @WebMethod(exclude = true)
    public EntityManager getEm() {
        return em;
    }

    @WebMethod(exclude = true)
    public void setEm(EntityManager em) {
        this.em = em;
    }

    @NotNull
    private IProjectRepository iProjectRepository;

    @PersistenceContext(unitName = "tm")
    EntityManager em;

    @NotNull
    private EntityToDTOConverter entityToDTOConverter = new EntityToDTOConverter();


    public ProjectService(IProjectRepository iProjectRepository) {
        this.iProjectRepository = iProjectRepository;

    }

    @WebMethod
    public Object getProjectRepository() {
        return iProjectRepository;
    }

    @WebMethod
    public void setProjectRepository(@WebParam(name = "projectRepository") final @NotNull Object iProjectRepository) {
        this.iProjectRepository = (IProjectRepository) iProjectRepository;
    }

    @WebMethod
    public void newProject(@WebParam(name = "projectName") @NotNull final String projectName, @WebParam(name = "userID") @NotNull final String userID) {
        em = UtilityHibernateConnection.connectHibernate();
        Project project = new Project(projectName, userID, Status.PLANNED, null, null);
        try {
            em.getTransaction().begin();
            iProjectRepository.persist(project);
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }

    }

    @WebMethod
    public List<ProjectDTO> showProjects(@WebParam(name = "userID") @NotNull final String userID) {
        em = UtilityHibernateConnection.connectHibernate();
        List<ProjectDTO> projectDTOs = new ArrayList<>();
        try {
            em.getTransaction().begin();
            for (Project project : iProjectRepository.getListProjectByUserId(userID)) {
                projectDTOs.add(entityToDTOConverter.convertToProjectDTO(project));
            }
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return projectDTOs;
    }

    @WebMethod
    public void changeProject(@WebParam(name = "projectName") @NotNull final String projectName, @WebParam(name = "projectNewName") @NotNull final String projectNewName) {
        em = UtilityHibernateConnection.connectHibernate();
        try {
            em.getTransaction().begin();
            Project project = iProjectRepository.findByName(projectName);
            project.setName(projectNewName);
            iProjectRepository.merge(project);
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }

    }

    @WebMethod
    public void deleteProject(@WebParam(name = "projectName") @NotNull final String projectName) {
        em = UtilityHibernateConnection.connectHibernate();
        try {
            em.getTransaction().begin();
            String projectID = iProjectRepository.findByName(projectName).getId();
            iProjectRepository.remove(projectID);
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }

    }

    @WebMethod
    public String findProjectID(@WebParam(name = "projectName") @NotNull final String projectName) {
        em = UtilityHibernateConnection.connectHibernate();
        String projectID = null;
        try {
            em.getTransaction().begin();
            projectID = iProjectRepository.findByName(projectName).getId();
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return projectID;
    }

    @WebMethod
    public List<String> sortProjectsByDateOfCreation(@WebParam(name = "userID") @NotNull final String userID) {
        List<String> projects = new ArrayList<>();
        for (Project project : iProjectRepository.sortByDateOfCreation(userID)) {
            projects.add(project.getName());
        }
        return projects;
    }

    @WebMethod
    public List<String> sortProjectsByStartDate(@WebParam(name = "userID") @NotNull final String userID) {
        List<String> projects = new ArrayList<>();
        for (Project project : iProjectRepository.sortByStartDate(userID)) {
            projects.add(project.getName());
        }
        return projects;
    }

    @WebMethod
    public List<String> sortProjectsByFinishDate(@WebParam(name = "userID") @NotNull final String userID) {
        List<String> projects = new ArrayList<>();
        for (Project project : iProjectRepository.sortByFinishDate(userID)) {
            projects.add(project.getName());
        }
        return projects;
    }

    @WebMethod
    public List<String> sortProjectsByStatus(@WebParam(name = "userID") @NotNull final String userID) {
        List<String> projects = new ArrayList<>();
        for (Project project : iProjectRepository.sortByStatus(userID)) {
            projects.add(project.getName());
        }
        return projects;
    }

    @WebMethod
    public List<String> globalProjectSearch(@WebParam(name = "text") @NotNull final String text) {
        List<String> projects = new ArrayList<>();
        for (Project project : iProjectRepository.globalSearch(text)) {
            projects.add(project.getName());
        }
        return projects;
    }

    @WebMethod
    public List<String> globalProjectSearchByDescription(@WebParam(name = "text") @NotNull final String text) {
        List<String> projects = new ArrayList<>();
        for (Project project : iProjectRepository.globalSearchByProjectDescription(text)) {
            projects.add(project.getName() + " ____ " + project.getProjectDescription());
        }
        return projects;
    }

    @WebMethod
    public void simpleProjectSave() throws IOException {
        FileOutputStream fos = new FileOutputStream("./target/savepr.bin");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        IProjectRepository projectRepository = (IProjectRepository) this.getIProjectRepository();
        oos.writeObject(projectRepository);
        oos.flush();
        oos.close();
    }

    @WebMethod
    public void simpleProjectLoad() throws IOException, ClassNotFoundException {
        FileInputStream fos = new FileInputStream("./target/savepr.bin");

        ObjectInputStream oin = new ObjectInputStream(fos);

        this.setIProjectRepository((IProjectRepository) oin.readObject());

    }

    @WebMethod
    public void jacksonJSONProjectSave() throws IOException {
        IProjectRepository projectRepository = ((IProjectRepository) this.getIProjectRepository());
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        objectMapper.writeValue(new File("./target/jacksonjsonprojects.json"), projectRepository);

    }

    @WebMethod
    public void jacksonJSONProjectLoad() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        this.setIProjectRepository((IProjectRepository) objectMapper.readValue(new File("./target/jacksonjsonprojects.json"), IProjectRepository.class));
    }

    @WebMethod
    public void jacksonXMLProjectSave() throws IOException {
        IProjectRepository projectRepository = ((IProjectRepository) this.getIProjectRepository());
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.findAndRegisterModules();
        xmlMapper.writeValue(new File("./target/jacksonXMLprojects.xml"), projectRepository);
    }

    @WebMethod
    public void jacksonXMLProjectLoad() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.findAndRegisterModules();
        File file = new File("./target/jacksonXMLprojects.xml");
        this.setIProjectRepository((IProjectRepository) xmlMapper.readValue(new File(file.getPath()), IProjectRepository.class));
    }

    @WebMethod
    public void jaxBJSONProjectSave() throws IOException, JAXBException {
        IProjectRepository projectRepository = ((IProjectRepository) this.getIProjectRepository());
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Map<String, Object> properties = new HashMap<String, Object>(1);
        properties.put("eclipselink.media-type", "application/json");
        JAXBContext jc = (JAXBContext) JAXBContext.newInstance(new Class[]{IProjectRepository.class}, properties);
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
        marshaller.marshal(projectRepository, new File("./target/tmpprojectdataJSON.json"));

    }

    @WebMethod
    public void jaxBJSONProjectLoad() throws IOException, JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Map<String, Object> properties = new HashMap<String, Object>(1);
        properties.put("eclipselink.media-type", "application/json");
        JAXBContext jc = (JAXBContext) JAXBContext.newInstance(new Class[]{IProjectRepository.class}, properties);

        Unmarshaller unmarshaller = jc.createUnmarshaller();

        this.setIProjectRepository((IProjectRepository) unmarshaller.unmarshal(new File("./target/tmpprojectdataJSON.json")));
    }

    @WebMethod
    public void jaxBXMLProjectSave() throws IOException, JAXBException {
        IProjectRepository projectRepository = ((IProjectRepository) this.getIProjectRepository());
        javax.xml.bind.JAXBContext context = JAXBContext.newInstance(IProjectRepository.class);
        Marshaller mar = context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.marshal(projectRepository, new File("./target/tmpprojectdata.xml"));

    }

    @WebMethod
    public void jaxBXMLProjectLoad() throws IOException, JAXBException {
        JAXBContext context = (JAXBContext) JAXBContext.newInstance(IProjectRepository.class);
        this.setIProjectRepository((IProjectRepository) context.createUnmarshaller().unmarshal(new FileReader("./target/tmpprojectdata.xml")));

    }
}


