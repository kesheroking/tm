package ru.laptiev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.persistence.jaxb.JAXBContext;
import org.hibernate.HibernateException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.AccessRole;
import ru.laptiev.tm.MD5Hash;
import ru.laptiev.tm.SignatureUtil;
import ru.laptiev.tm.entity.Session;
import ru.laptiev.tm.entity.User;
import ru.laptiev.tm.repository.IUserRepository;
import ru.laptiev.tm.util.EntityToDTOConverter;
import ru.laptiev.tm.util.UtilityHibernateConnection;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@WebService(name = "UserService")
public final class UserService implements Serializable {
    @NotNull
    private IUserRepository iUserRepository;
    @PersistenceContext(unitName = "tm")
    EntityManager em;

    @NotNull
    private EntityToDTOConverter entityToDTOConverter = new EntityToDTOConverter();

    @WebMethod(exclude = true)
    public IUserRepository getIUserRepository() {
        return iUserRepository;
    }

    @WebMethod(exclude = true)
    public void setIUserRepository(IUserRepository iUserRepository) {
        this.iUserRepository = iUserRepository;
    }

    @WebMethod(exclude = true)
    public EntityManager getEm() {
        return em;
    }

    @WebMethod(exclude = true)
    public void setEm(EntityManager em) {
        this.em = em;
    }

    @WebMethod
    public Object getUserRepository() {
        return iUserRepository;
    }

    @WebMethod
    public void setUserRepository(
            @WebParam(name = "userRepository") final @NotNull Object iUserRepository) {
        this.iUserRepository = (IUserRepository) iUserRepository;
    }


    public UserService(@NotNull final IUserRepository iUserRepository) {
        this.iUserRepository = iUserRepository;
    }

    @WebMethod
    public void registration(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password,
            @WebParam(name = "accessRole") @NotNull final String role
    ) {
        em = UtilityHibernateConnection.connectHibernate();
        try {

            em.getTransaction().begin();
            if (iUserRepository.checkPresence(iUserRepository.findByName(login))) {
                System.out.println("Пользователь с таким именем уже зарегестрирован!");
            } else {
                iUserRepository.persist(new User(login, password, (role.equals("admin") ? AccessRole.ADMIN : AccessRole.REGULAR_USER)));
            }
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @WebMethod
    public Session authorization(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) {
        em = UtilityHibernateConnection.connectHibernate();
        User user = null;
        try {
            em.getTransaction().begin();
            user = iUserRepository.findByName(login);
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }

        if (user == null) {
            System.out.println("User " + login + " doesn't exists!");
            return null;
        }
        if (user.getPassword().equals(MD5Hash.getMD5Hash(password))) {
            System.out.println("Logged in successfully!");
            Session session = new Session(10000000L, user.getId(), SignatureUtil.sign(user, "123", 100));
            System.out.println(user.getId() + "  " + user.getAccessRole() + " " + user.getName() + " " + user.getPassword());
            return session;
        } else {
            System.out.println("Incorrect login/password!");
            return null;

        }
    }

    @WebMethod
    public boolean sessionCheck(
            @WebParam(name = "signature") @Nullable final String signature,
            @WebParam(name = "UserID") @Nullable final String UserID) {
        em = UtilityHibernateConnection.connectHibernate();
        String currentSignature = null;
        try {
            em.getTransaction().begin();
            currentSignature = SignatureUtil.sign(iUserRepository.findOne(UserID), "123", 100);
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        if (signature.equals(currentSignature))
            return true;
        else return false;

    }

    @WebMethod
    public void newPassword(
            @WebParam(name = "userID") @NotNull final String userID,
            @WebParam(name = "password") @NotNull final String password
    ) {
        em = UtilityHibernateConnection.connectHibernate();
        try {
            em.getTransaction().begin();
            User user = iUserRepository.findOne(userID);
            user.setPassword(MD5Hash.getMD5Hash(password));
            iUserRepository.merge(user);
            em.getTransaction().commit();
        } catch (HibernateException e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @WebMethod
    public void simpleUserSave() throws IOException {
        FileOutputStream fos = new FileOutputStream("./target/saveus.bin");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        IUserRepository iUserRepository = (IUserRepository) this.getIUserRepository();
        oos.writeObject(iUserRepository);
        oos.flush();
        oos.close();
    }

    @WebMethod
    public void simpleUserLoad() throws IOException, ClassNotFoundException {
        FileInputStream fos = new FileInputStream("./target/saveus.bin");

        ObjectInputStream oin = new ObjectInputStream(fos);

        this.setIUserRepository((IUserRepository) oin.readObject());

    }

    @WebMethod
    public void jacksonJSONUserSave() throws IOException {
        IUserRepository iUserRepository = ((IUserRepository) this.getIUserRepository());
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        objectMapper.writeValue(new File("./target/jacksonjsonusers.json"), iUserRepository);
    }

    @WebMethod
    public void jacksonJSONUserLoad() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        this.setIUserRepository((IUserRepository) objectMapper.readValue(new File("./target/jacksonjsonusers.json"), IUserRepository.class));
    }

    @WebMethod
    public void jacksonXMLUserSave() throws IOException {
        IUserRepository iUserRepository = ((IUserRepository) this.getIUserRepository());
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.findAndRegisterModules();
        xmlMapper.writeValue(new File("./target/jacksonXMLusers.xml"), iUserRepository);
    }

    @WebMethod
    public void jacksonXMLUserLoad() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.findAndRegisterModules();
        File file = new File("./target/jacksonXMLusers.xml");
        this.setIUserRepository((IUserRepository) xmlMapper.readValue(new File(file.getPath()), IUserRepository.class));

    }

    @WebMethod
    public void jaxBJSONUserSave() throws IOException, JAXBException {
        IUserRepository iUserRepository = ((IUserRepository) this.getIUserRepository());
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Map<String, Object> properties = new HashMap<String, Object>(1);
        properties.put("eclipselink.media-type", "application/json");
        JAXBContext jc3 = (JAXBContext) JAXBContext.newInstance(new Class[]{IUserRepository.class}, properties);
        Marshaller marshaller3 = jc3.createMarshaller();
        marshaller3.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
        marshaller3.marshal(iUserRepository, new File("./target/tmpuserdataJSON.json"));
    }

    @WebMethod
    public void jaxBJSONUserLoad() throws IOException, JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Map<String, Object> properties = new HashMap<String, Object>(1);
        properties.put("eclipselink.media-type", "application/json");
        JAXBContext jc3 = (JAXBContext) JAXBContext.newInstance(new Class[]{IUserRepository.class}, properties);
        Unmarshaller unmarshaller3 = jc3.createUnmarshaller();
        this.setIUserRepository((IUserRepository) unmarshaller3.unmarshal(new File("./target/tmpuserdataJSON.json")));
    }

    @WebMethod
    public void jaxBXMLUserSave() throws IOException, JAXBException {
        IUserRepository iUserRepository = ((IUserRepository) this.getIUserRepository());
        javax.xml.bind.JAXBContext context3 = JAXBContext.newInstance(IUserRepository.class);
        Marshaller mar3 = context3.createMarshaller();
        mar3.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar3.marshal(iUserRepository, new File("./target/tmpuserdata.xml"));
    }

    @WebMethod
    public void jaxBXMLUserLoad() throws IOException, JAXBException {
        JAXBContext context3 = (JAXBContext) JAXBContext.newInstance(IUserRepository.class);
        this.setIUserRepository((IUserRepository) context3.createUnmarshaller()
                .unmarshal(new FileReader("./target/tmpuserdata.xml")));
    }
}
