package ru.laptiev.tm;


import javax.xml.ws.Endpoint;

public final class App {
    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
        Endpoint.publish("http://0.0.0.0:8080/ProjectEndPoint?wsdl", bootstrap.getProjectService());
        Endpoint.publish("http://0.0.0.0:8080/TaskEndPoint?wsdl", bootstrap.getTaskService());
        Endpoint.publish("http://0.0.0.0:8080/UserEndPoint?wsdl", bootstrap.getUserService());
        Endpoint.publish("http://0.0.0.0:8080/SessionEndPoint?wsdl", bootstrap.getSessionService());
    }

}
