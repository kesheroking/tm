package ru.laptiev.tm;

import org.jetbrains.annotations.NotNull;

public enum AccessRole {
    ADMIN("Admin"),
    REGULAR_USER("User");
    private final String role;

    private AccessRole(@NotNull final String role) {
        this.role = role;
    }

    public String displayName() {
        return this.role;
    }

}



