package ru.laptiev.tm;

import org.jetbrains.annotations.NotNull;

public enum Status {
    PLANNED("Planned"), IN_PROGRESS("In progress"), DONE("Done");

    private final String status;

    private Status(@NotNull final String status) {
        this.status = status;
    }

    public String displayName() {
        return this.status;
    }

}


