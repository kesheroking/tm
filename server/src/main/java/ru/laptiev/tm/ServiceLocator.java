package ru.laptiev.tm;

import ru.laptiev.tm.service.ProjectService;
import ru.laptiev.tm.service.TaskService;
import ru.laptiev.tm.service.UserService;

public interface ServiceLocator {


    ProjectService getProjectService();

    TaskService getTaskService();


    UserService getUserService();


}
