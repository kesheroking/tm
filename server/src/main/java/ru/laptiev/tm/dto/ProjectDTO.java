package ru.laptiev.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.bytebuddy.asm.Advice;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.Status;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
public class ProjectDTO {
    private final @NotNull String id;
    private @NotNull String name;
    private @NotNull String user_id;
    private @NotNull Status status;
    private @Nullable String description;
    private @Nullable LocalDateTime dateBegin;
    private @Nullable LocalDateTime dateEnd;

}
