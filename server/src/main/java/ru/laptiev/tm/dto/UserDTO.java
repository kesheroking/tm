package ru.laptiev.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.AccessRole;

import java.time.LocalDateTime;
@Getter
@Setter
@AllArgsConstructor
public class UserDTO {
    private final @NotNull String id;
    private @NotNull String name;
    private @NotNull String passwordHash;
    private @NotNull AccessRole role;
}
