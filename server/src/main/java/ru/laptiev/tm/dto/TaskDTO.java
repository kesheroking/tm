package ru.laptiev.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.Status;

import java.time.LocalDateTime;
@Getter
@Setter
@AllArgsConstructor

public class TaskDTO {
    private final @NotNull String id;
    private @NotNull String name;
    private @NotNull String project_id;
    private @NotNull String user_id;
    private @NotNull Status status;
}
