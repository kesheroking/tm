package ru.laptiev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.entity.User;

public interface IUserRepository extends IAbstractRepository<User> {

    public User removeByLogin(@NotNull String userLogin);

    boolean checkPresence(@NotNull User user);
}
