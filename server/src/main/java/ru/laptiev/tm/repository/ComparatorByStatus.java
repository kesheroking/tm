package ru.laptiev.tm.repository;

import ru.laptiev.tm.entity.AbstractEntity;

import java.util.Comparator;

public class ComparatorByStatus<T extends AbstractEntity> implements Comparator<T> {
    @Override
    public int compare(T type1, T type2) {
        return type1.getStatus().compareTo(type2.getStatus());
    }
}
