package ru.laptiev.tm.repository;

import ru.laptiev.tm.entity.Session;

public interface ISessionRepository extends IAbstractRepository<Session> {

}
