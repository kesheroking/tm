package ru.laptiev.tm.repository;

import ru.laptiev.tm.entity.AbstractSecondEntity;

import java.time.LocalDateTime;
import java.util.Comparator;

public class ComparatorByFinishDate<T extends AbstractSecondEntity> implements Comparator<T> {
    @Override
    public int compare(T type1, T type2) {
        LocalDateTime localDateTime1 = type1.getDateEnd();
        LocalDateTime localDateTime2 = type2.getDateEnd();
        return localDateTime1.compareTo(localDateTime2);
    }
}
