package ru.laptiev.tm.repository;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.entity.Project;
import ru.laptiev.tm.util.UtilityHibernateConnection;
import ru.laptiev.tm.entity.Task;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "tasks")
@JsonRootName(value = "tasks")
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
    @PersistenceContext(unitName = "tm")
    EntityManager em;

    {
        em = UtilityHibernateConnection.connectHibernate();
    }

    @SneakyThrows
    @Transactional
    public void persist(@NotNull final Task task) {
        Project project = em.find(Project.class, task.getProjectID());
        task.setMotherProject(project);
        project.addTask(task);
        em.persist(task);
        em.merge(project);
    }


    @SneakyThrows
    public boolean checkPresence(@NotNull final Task task) {
        Task task1 = em.find(Task.class, task.getId());
        if (task1.equals(null)) {
            return false;
        } else {
            return true;
        }
    }

    @SneakyThrows
    public void merge(@NotNull final Task task) {
        if (checkPresence(task)) {
            em.merge(task);
        }
    }

    @SneakyThrows
    public void remove(@NotNull final String taskID) {
        Task task1 = em.find(Task.class, taskID);
        em.remove(task1);

    }

    @NotNull
    @SneakyThrows
    public List<Task> getListTaskByProjectId(final String projectId) {
        Project project = em.createQuery("SELECT project from Project project WHERE project.id = ?1", Project.class)
                .setParameter(1, projectId)
                .getSingleResult();
        return project.getTasks();
    }

    @Override
    @Nullable
    @SneakyThrows
    public Task findByName(@NotNull final String name) {
        Task task = (Task) em.createQuery("from Task WHERE name = ?1")
                .setParameter(1, name)
                .getSingleResult();
        return task;
    }

    @Override
    public List<Task> sortByStartDate(@NotNull String userId) {
        return null;
    }

    @Override
    public List<Task> sortByFinishDate(@NotNull String userId) {
        return null;
    }

    @SneakyThrows
    public void removeByProjectID(@NotNull final String projectID) {
        List<Task> list = getListTaskByProjectId(projectID);
        for (Task task : list) {
            em.remove(task);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<Task> globalSearch(@NotNull final String text) {
        return (List<Task>) em.createQuery("from Task WHERE name = '%',?1, '%'")
                .setParameter(1, text)
                .getResultList();
    }

    @Nullable
    @SneakyThrows
    public List<Task> globalSearchByTaskDescription(@NotNull final String text) {
        return (List<Task>) em.createQuery("from Task WHERE descriprion ='%', ?1, '%'")
                .setParameter(1, text)
                .getResultList();
    }
}






