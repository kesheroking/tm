package ru.laptiev.tm.repository;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import ru.laptiev.tm.entity.AbstractEntity;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class AbstractRepositoryJsonSerializer<T extends AbstractRepository<U>, U extends AbstractEntity> extends JsonSerializer<T> {

    @Override
    public void serialize(T value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
//        ToXmlGenerator xmlGen = (ToXmlGenerator) gen;
//        xmlGen.writeStartObject();
//        xmlGen.writeObjectFieldStart("repositoryMap");
//        for (Map.Entry<String, U> entry : value.getRepositoryMap().entrySet()) {
//            xmlGen.writeObjectFieldStart("entry");
//            xmlGen.writeObjectFieldStart("key");
//            xmlGen.writeString(entry.getKey());
//            xmlGen.writeEndObject();
//            xmlGen.writeObjectFieldStart("entry");
//            writeAttributes(xmlGen, entry.getKey());
//            Object entity = entry.getValue();
//            List<Field> fields = getFields(entity);
//            for (Field field : fields) {
//                field.setAccessible(true);
//                try {
//                    if (field.get(entity) != null) {
//                        xmlGen.writeStringField(field.getName(), field.get(entity).toString());
//                    }
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//            xmlGen.writeEndObject();
//            xmlGen.writeEndObject();
//        }
//        xmlGen.writeEndObject();
    }

    private <T> List<Field> getFields(T t) {
        List<Field> fields = new ArrayList<>();
        Class clazz = t.getClass();
        while (clazz != Object.class) {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

    private void writeAttributes(ToXmlGenerator gen, String key) throws IOException {
        gen.setNextIsAttribute(true);
        gen.writeFieldName("key");
        gen.writeString(key);
        gen.setNextIsAttribute(false);
    }
}