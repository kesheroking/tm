package ru.laptiev.tm.repository;

import ru.laptiev.tm.entity.AbstractEntity;
import ru.laptiev.tm.entity.AbstractSecondEntity;

import java.time.LocalDateTime;
import java.util.Comparator;

public class ComparatorByStartDate<T extends AbstractSecondEntity> implements Comparator<T> {
    @Override
    public int compare(T type1, T type2) {
        LocalDateTime localDateTime1 = type1.getDateBegin();
        LocalDateTime localDateTime2 = type2.getDateBegin();
        return localDateTime1.compareTo(localDateTime2);
    }
}
