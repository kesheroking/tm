package ru.laptiev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.entity.Project;

import java.io.Serializable;
import java.util.List;

public interface IProjectRepository extends IAbstractRepository<Project> {
     List<Project> getListProjectByUserId(final String userId);
    List<Project> globalSearchByProjectDescription(@NotNull final String text);

}
