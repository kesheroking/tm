package ru.laptiev.tm.repository;

import ru.laptiev.tm.entity.AbstractEntity;

import java.time.LocalDateTime;
import java.util.Comparator;

public class ComparatorByDateOfCreation<T extends AbstractEntity> implements Comparator<T> {
    public int compare(T type1, T type2) {
        LocalDateTime localDateTime1 = type1.getDateOfCreation();
        LocalDateTime localDateTime2 = type2.getDateOfCreation();
        return localDateTime1.compareTo(localDateTime2);
    }
}
