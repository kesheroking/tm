package ru.laptiev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.entity.Task;

import java.io.Serializable;
import java.util.List;

public interface ITaskRepository extends IAbstractRepository<Task> {

    void removeByProjectID(@NotNull String projectID);

//    List<Task> findByProjectID(@NotNull String projectID, @NotNull String userID);

    List<Task> globalSearchByTaskDescription(@NotNull final String text);
    List<Task> getListTaskByProjectId(@NotNull final String projectId);
}
