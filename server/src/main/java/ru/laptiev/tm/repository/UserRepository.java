package ru.laptiev.tm.repository;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.util.UtilityHibernateConnection;
import ru.laptiev.tm.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "users")
@JsonRootName(value = "users")

public final class UserRepository extends AbstractRepository<User> implements IUserRepository{
    @PersistenceContext(unitName = "tm")
    EntityManager em;

    {
        em = UtilityHibernateConnection.connectHibernate();
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByName(@NotNull final String name) {
        try {
            User user = (User) em.createQuery("SELECT user from User user where user.name = ?1")
                    .setParameter(1, name)
                    .getSingleResult();
            return user;
        } catch (NoResultException e) {
            return new User();
        }

    }

    @Override
    public List<User> sortByStartDate(@NotNull String userId) {
        return null;
    }

    @Override
    public List<User> sortByFinishDate(@NotNull String userId) {
        return null;
    }
    public User removeByLogin(@NotNull final String userLogin) {
        em.getTransaction().begin();
        User user = em.find(User.class, findByName(userLogin).getId());
        em.remove(user);
        em.getTransaction().commit();
        return user;
    }
    @SneakyThrows
    public boolean checkPresence(@NotNull final User user) {
        User user1 = em.find(User.class, user.getId());
        if (user1 == null) {
            return false;
        } else {
            return true;
        }
    }


    @SneakyThrows
    public void persist(@NotNull final User user) {
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
    }

    @SneakyThrows
    public void merge(@NotNull final User user) {
        if (this.checkPresence(user)) {
            em.getTransaction().begin();
            em.merge(user);
            em.getTransaction().commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public User findOne(@NotNull final String id) {
        User user = (User) em.createQuery("from User WHERE id = ?1")
                .setParameter(1, id)
                .getSingleResult();
        return user;
    }

    @Override
    public List<User> globalSearch(@NotNull String text) {
        return (List<User>) em.createQuery("from Project WHERE name = '%',?1, '%'")
                .setParameter(1, text)
                .getResultList();
    }
}

