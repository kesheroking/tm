package ru.laptiev.tm.repository;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.util.UtilityHibernateConnection;
import ru.laptiev.tm.entity.Project;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;


@XmlRootElement(name = "projects")
@JsonRootName(value = "projects")
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
    @PersistenceContext(unitName = "tm")
    EntityManager em;

    {
        em = UtilityHibernateConnection.connectHibernate();
    }


    @NotNull
    @SneakyThrows
    public List<Project> getListProjectByUserId(final String userId) {
        try { return (List<Project>) em.createQuery(" FROM Project WHERE user_id = ?1")
                .setParameter(1, userId)
                .getResultList();
        } catch (
                NoResultException e) {
            return new ArrayList<>();
        }
    }


    @SneakyThrows
    public boolean checkPresence(final Project project) {
        Project project1 = em.find(Project.class, project.getId());
        return project1 != null;
    }

    @SneakyThrows
    public void persist(@NotNull final Project project) {
        em.persist(project);
    }

    @SneakyThrows
    public void merge(@NotNull final Project project) {
        if (checkPresence(project)) {
            em.merge(project);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public Project findByName(@NotNull final String name) {
        try {
            Project project = (Project) em.createQuery("from Project WHERE name = ?1")
                    .setParameter(1, name)
                    .getSingleResult();
            return project;
        } catch (
                NoResultException e) {
            return new Project();
        }
    }

    @Override
    public List<Project> sortByStartDate(@NotNull String userId) {
        return null;
    }

    @Override
    public List<Project> sortByFinishDate(@NotNull String userId) {
        return null;
    }


    @SneakyThrows
    public void remove(@NotNull final String projectID) {
        em.remove(em.find(Project.class, projectID));
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<Project> globalSearch(@NotNull final String text) {

       try{
           return  (List<Project>) em.createQuery("from Project WHERE name = '%',?1, '%'")
                   .setParameter(1, text)
                   .getResultList();
       }
       catch (NoResultException e){
           return new ArrayList<>();
       }
    }

    @Nullable
    @SneakyThrows
    public List<Project> globalSearchByProjectDescription(@NotNull final String text) {
       try{
           return (List<Project>) em.createQuery("from Project WHERE descriprion ='%', ?1, '%'")
                   .setParameter(1, text)
                   .getResultList();
       }
       catch (NoResultException e){
           return new ArrayList<>();
       }
    }

}

