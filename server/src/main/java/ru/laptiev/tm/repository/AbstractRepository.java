package ru.laptiev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.entity.AbstractEntity;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<T extends AbstractEntity> implements IAbstractRepository<T> {


    public List<T> findAll(@NotNull final String userId) {
//        return new ArrayList<T>(repositoryMap.values().stream().filter(x -> x.getUserId().equals(userId)).collect(Collectors.toList()));
        return null;
    }

    public T findOne(@Nullable final String id) {
        return null;
    }

    public void persist(@NotNull final T type) {

    }

    public void merge(@NotNull final T type) {
    }

    public void remove(@NotNull final String id) {
    }

    public void removeAll() {
    }
    public boolean checkPresence(@NotNull final T type){
return true;
    }

    public T findByName(@NotNull final String projectName) {
        return null;
    }

    public void removeByName(@NotNull final String projectName) {
    }

    public List<T> sortByDateOfCreation(@NotNull final String userId) {
        ComparatorByDateOfCreation<T> comparator = new ComparatorByDateOfCreation<>();
        return findAll(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

//    public List<T> sortByStartDate(@NotNull final String userId) {
//        ComparatorByStartDate<T> comparator = new ComparatorByStartDate<>();
//        return findAll(userId).stream().sorted(comparator).collect(Collectors.toList());
//    }

//    public List<T> sortByFinishDate(@NotNull final String userId) {
//        ComparatorByFinishDate<T> comparator = new ComparatorByFinishDate<>();
//        return findAll(userId).stream().sorted(comparator).collect(Collectors.toList());
//    }

    public List<T> sortByStatus(@NotNull final String userId) {
        ComparatorByStatus<T> comparator = new ComparatorByStatus<>();
        return findAll(userId).stream().sorted(comparator).collect(Collectors.toList());
    }


}

