package ru.laptiev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.entity.AbstractEntity;
import ru.laptiev.tm.entity.User;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.List;

public interface IAbstractRepository<T extends AbstractEntity>  {

 List<T> findAll(@NotNull String userId);

 T findOne(@NotNull String projectID);

 void persist(@NotNull T type);

 void merge(@NotNull T type);

 void remove(@NotNull String projectID);

 void removeAll();

 T findByName(@NotNull String name);

 void removeByName(@NotNull String name);

 boolean checkPresence(T type);
; List<T> sortByDateOfCreation(@NotNull String userID);

 List<T> sortByStartDate(@NotNull String userId);

 List<T> sortByFinishDate(@NotNull String userId);

 List<T> sortByStatus(@NotNull String userId);

 List<T> globalSearch(@NotNull String text);


}
