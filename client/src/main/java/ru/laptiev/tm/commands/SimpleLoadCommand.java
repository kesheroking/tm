package ru.laptiev.tm.commands;

import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SimpleLoadCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "simple load";
    }

    @Override
    public String description() {
        return "Deserialize existing app data";
    }

    @Override
    public void execute() throws Exception {

        bootstrap.getIProjectService().simpleProjectLoad();
        bootstrap.getITaskService().simpleTaskLoad();
        bootstrap.getIUserService().simpleUserLoad();

    }
}
