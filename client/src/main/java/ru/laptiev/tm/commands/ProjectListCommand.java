package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
public final class ProjectListCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "show projects";
    }

    @Override
    public String description() {
        return "Show existing projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println(bootstrap.getIProjectService().showProjects(this.bootstrap.getCurrentUserID()));
    }
}
