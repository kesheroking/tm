package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter

public final class TaskChangeCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "change task";
    }

    @Override
    public String description() {
        return "Change existing task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.print("Enter task: ");
        String oldTask = bootstrap.getTerminalService().readLine();
        System.out.print("Enter new task: ");
        String newTask = bootstrap.getTerminalService().readLine();
        bootstrap.getITaskService().changeTask(oldTask, newTask);
    }
}
