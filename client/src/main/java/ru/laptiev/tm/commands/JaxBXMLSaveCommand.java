package ru.laptiev.tm.commands;

import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class JaxBXMLSaveCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "jaxb xml save";
    }

    @Override
    public String description() {
        return "Marshall current app data to XML";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getIProjectService().jaxBXMLProjectSave();
        bootstrap.getITaskService().jaxBXMLTaskSave();
        bootstrap.getIUserService().jaxBXMLUserSave();
    }
}
