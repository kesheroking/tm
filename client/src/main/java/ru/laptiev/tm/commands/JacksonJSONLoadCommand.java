package ru.laptiev.tm.commands;

import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class JacksonJSONLoadCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "jackson json load";
    }

    @Override
    public String description() {
        return "Unmarshall JSON data into Task Manager";
    }

    @Override
    public void execute() throws Exception {

        bootstrap.getIProjectService().jacksonJSONProjectLoad();
        bootstrap.getITaskService().jacksonJSONTaskLoad();
        bootstrap.getIUserService().jacksonJSONUserLoad();
    }

}