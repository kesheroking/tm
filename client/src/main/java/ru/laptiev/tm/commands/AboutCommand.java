package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.net.URL;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.Manifest;

@Getter
public class AboutCommand extends AbstractCommand {

    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));


    @Override
    public String command() {
        return "about";
    }

    @Override
    public String description() {
        return "Show current build number";
    }

    @Override
    public void execute() throws Exception {
        Enumeration<URL> resources = getClass().getClassLoader()
                .getResources("META-INF/MANIFEST.MF");
        Manifest manifest = new Manifest(resources.nextElement().openStream());
        manifest.getMainAttributes().get("buildnumber");
        System.out.println("Build number: " + manifest.getMainAttributes().get("buildnumber"));
    }
}
