package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
public class GlobalProjectSearchCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));


    @Override
    public String command() {
        return "global project search";
    }

    @Override
    public String description() {
        return "Global search for projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("What are you looking for?");
        String text = bootstrap.getTerminalService().readLine();
        System.out.println("Matches in project names:");
        for (String string :
                bootstrap.getIProjectService().globalProjectSearch(text)){
            System.out.println(string);
        }
        System.out.println("Matches in project descriprions:");
        for (String string :
                bootstrap.getIProjectService().globalProjectSearchByDescription(text)){
            System.out.println(string);
        }}
}

