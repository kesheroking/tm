package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
public final class UserChangePasswordCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "change password";
    }

    @Override
    public String description() {
        return "Change your password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.print("Enter new password: ");
        String newPassword = bootstrap.getTerminalService().readLine();
        bootstrap.getIUserService().newPassword(this.bootstrap.getCurrentUserID(), newPassword);
    }
}
