package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter

public final class TaskListCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "show tasks";
    }

    @Override
    public String description() {
        return "Show existing tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.print("Enter project name: ");
        String projectName = bootstrap.getTerminalService().readLine();
        String projectID = bootstrap.getIProjectService().findProjectID(projectName);
        System.out.println(bootstrap.getITaskService().showTasks(projectID));
    }
}
