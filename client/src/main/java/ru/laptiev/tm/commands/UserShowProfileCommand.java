package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
public final class UserShowProfileCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN));

    @Override
    public String command() {
        return "show profile";
    }

    @Override
    public String description() {
        return "Look at current User profile.";
    }

    @Override
    public void execute() throws Exception {

//        System.out.println("Login: " + bootstrap.getCurrentUser().getName() + "\n" +
//                "UserID: " + bootstrap.getCurrentUser().getId() + "\n" +
//                "Access Role: " + bootstrap.getCurrentUser().getAccessRole());
    }
}
