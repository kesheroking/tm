package ru.laptiev.tm.commands;

import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.AccessRole;
import ru.laptiev.tm.Bootstrap;
import ru.laptiev.tm.service.ServiceLocator;

import java.util.Set;

public abstract class AbstractCommand {
    @NotNull
    protected Bootstrap bootstrap;
    @NotNull
    protected ServiceLocator serviceLocator;

    @NotNull
    public Set<AccessRole> accessRole;

    public Set<AccessRole> getAccessRole() {
        return accessRole;
    }

    public void setBootstrap(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;
}
