package ru.laptiev.tm.commands;

import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class JaxBXMLLoadCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "jaxb xml load";
    }

    @Override
    public String description() {
        return "Unmarshall XML data into Task Manager";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getIProjectService().jaxBXMLProjectLoad();
        bootstrap.getITaskService().jaxBXMLTaskLoad();
        bootstrap.getIUserService().jaxBXMLUserLoad();
    }
}
