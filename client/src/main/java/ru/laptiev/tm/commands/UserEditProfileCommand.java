package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter

public final class UserEditProfileCommand extends AbstractCommand {

    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "edit profile";
    }

    @Override
    public String description() {
        return "Change current User profile";
    }

    @Override
    public void execute() throws Exception {
//        System.out.print("Enter new User login or press enter: ");
//        String command = serviceLocator.getTerminalService().readLine();
//        serviceLocator.getCurrentUser().setName(command);
    }
}
