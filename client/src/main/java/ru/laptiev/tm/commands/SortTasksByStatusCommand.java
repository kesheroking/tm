package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
public class SortTasksByStatusCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "sort tasks by status";
    }

    @Override
    public String description() {
        return "Sort tasks by status";
    }

    @Override
    public void execute() throws Exception {
        System.out.println(bootstrap.getITaskService().sortTasksByStatus(this.bootstrap.getCurrentUserID()));
    }
}