package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;
import ru.laptiev.tm.entity.Session;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
public final class UserAuthorisationCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "log in";
    }

    @Override
    public String description() {
        return "Log in to existing account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.print("Enter login: ");
        String login = bootstrap.getTerminalService().readLine();
        System.out.print("Enter password: ");
        String password = bootstrap.getTerminalService().readLine();
        Session session = bootstrap.getIUserService().authorization(login, password);
        if (session != null) {
            bootstrap.setCurrentSession(bootstrap.getISessionService().openSession(login, password));
            bootstrap.setCurrentUserID(session.getUserID());
            if (!bootstrap.getCurrentSession().equals(null))
                System.out.println("Logged in successfully!");

        }

    }
}
