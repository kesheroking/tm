package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter

public final class TaskDeleteCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "delete task";
    }

    @Override
    public String description() {
        return "Delete existing task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.print("Enter task: ");
        String taskName = bootstrap.getTerminalService().readLine();
        bootstrap.getITaskService().deleteTask(taskName);
    }
}
