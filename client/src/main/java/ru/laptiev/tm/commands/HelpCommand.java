package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
public final class HelpCommand extends AbstractCommand {


    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands.";
    }


    @Override
    public void execute() throws Exception {
        for (final AbstractCommand command :
                this.bootstrap.getCommands()) {
            System.out.println(command.command() + ": "
                    + command.description());
        }
    }
}

