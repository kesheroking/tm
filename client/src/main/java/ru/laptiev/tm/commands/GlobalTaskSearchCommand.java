package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
public class GlobalTaskSearchCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));


    @Override
    public String command() {
        return "global task search";
    }

    @Override
    public String description() {
        return "Global search for tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("What are you looking for?");
        String text = bootstrap.getTerminalService().readLine();
        System.out.println("Matches in task names:");
        for (String string :
        bootstrap.getITaskService().globalTaskSearch(text)){
            System.out.println(string);
        }
        System.out.println("Matches in task descriprions:");
        for (String string :
        bootstrap.getITaskService().globalTaskSearchByDescription(text)){
            System.out.println(string);
        }
    }
}


