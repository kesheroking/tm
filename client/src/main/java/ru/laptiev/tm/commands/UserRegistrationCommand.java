package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter

public final class UserRegistrationCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "new user";
    }

    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.print("Enter your login: ");
        String login = bootstrap.getTerminalService().readLine();
        System.out.print("Enter your password: ");
        String password = bootstrap.getTerminalService().readLine();
        bootstrap.getIUserService().registration(login, password, "user");
    }
}
