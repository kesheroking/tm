package ru.laptiev.tm.commands;

import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SimpleSaveCommmand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "simple save";
    }

    @Override
    public String description() {
        return "Serialize current app data";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getIProjectService().simpleProjectSave();
        bootstrap.getITaskService().simpleTaskSave();
        bootstrap.getIUserService().simpleUserSave();
    }
}