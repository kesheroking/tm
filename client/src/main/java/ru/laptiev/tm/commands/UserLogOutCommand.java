package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
public final class UserLogOutCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "log out";
    }

    @Override
    public String description() {
        return "Log out your account";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.setCurrentSession(null);
    }
}
