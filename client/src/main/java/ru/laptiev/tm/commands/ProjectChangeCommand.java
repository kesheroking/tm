package ru.laptiev.tm.commands;

import lombok.Getter;
import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter

public final class ProjectChangeCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));


    @Override
    public String command() {
        return "change project";
    }

    @Override
    public String description() {
        return "Change existing project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.print("Enter project name: ");
        String oldName = bootstrap.getTerminalService().readLine();
        System.out.print("Enter new project name: ");
        String newName = bootstrap.getTerminalService().readLine();
        bootstrap.getIProjectService().changeProject(oldName, newName);
    }
}
