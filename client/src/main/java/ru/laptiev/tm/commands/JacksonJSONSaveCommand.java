package ru.laptiev.tm.commands;

import ru.laptiev.tm.AccessRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class JacksonJSONSaveCommand extends AbstractCommand {
    public final Set<AccessRole> accessRole = new HashSet<>(Arrays.asList(AccessRole.ADMIN, AccessRole.REGULAR_USER));

    @Override
    public String command() {
        return "jackson json save";
    }

    @Override
    public String description() {
        return "Marshall current app data to JSON";
    }


    @Override
    public void execute() throws Exception {

        bootstrap.getIProjectService().jacksonJSONProjectSave();
        bootstrap.getITaskService().jacksonJSONTaskSave();
        bootstrap.getIUserService().jacksonJSONUserSave();

    }
}
