package ru.laptiev.tm;


import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.laptiev.tm.commands.AbstractCommand;

import java.util.Set;

public final class App {
    private static final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.laptiev.tm").getSubTypesOf(AbstractCommand.class);

    public static void main(String[] args) {
        @NotNull
        Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.init(classes);
        } catch (Exception e) {
            System.out.println("Error!");
            e.printStackTrace();
        }
    }
}
