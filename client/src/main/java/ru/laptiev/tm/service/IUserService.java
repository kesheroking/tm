package ru.laptiev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserService {
    String PORT = "UserServiceService";
    String NAME = "http://service.tm.laptiev.ru/";
    String WSDL = "http://0.0.0.0:8080/UserEndPoint?wsdl";

    @WebMethod
    public Object getUserRepository();

    @WebMethod
    public void setUserRepository(
            @WebParam(name = "userRepository") final @NotNull Object userRepository);

    @WebMethod
    void registration(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password,
            @WebParam(name = "accessRole") @NotNull final String role);

    @WebMethod
    public boolean sessionCheck(
            @WebParam(name = "signature") @Nullable final String signature,
            @WebParam(name = "UserID") @Nullable final String UserID);

    @WebMethod
    Session authorization(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password);

    @WebMethod
    void newPassword(
            @WebParam(name = "userID") @NotNull final String id,
            @WebParam(name = "password") @NotNull final String password);

    @WebMethod
    public void simpleUserSave();

    @WebMethod
    public void simpleUserLoad();

    @WebMethod
    public void jacksonJSONUserSave();

    @WebMethod
    public void jacksonJSONUserLoad();

    @WebMethod
    public void jacksonXMLUserSave();

    @WebMethod
    public void jacksonXMLUserLoad();

    @WebMethod
    public void jaxBJSONUserSave();

    @WebMethod
    public void jaxBJSONUserLoad();

    @WebMethod
    public void jaxBXMLUserSave();

    @WebMethod
    public void jaxBXMLUserLoad();

}
