package ru.laptiev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.AccessRole;
import ru.laptiev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService
public interface ISessionService {
    String PORT = "SessionServiceService";
    String NAME = "http://service.tm.laptiev.ru/";
    String WSDL = "http://0.0.0.0:8080/SessionEndPoint?wsdl";


    public void closeSession();


    public void closeSessionAll();


    public void getListSession();


    public void getUser();

    @WebMethod
    public Session openSession(@WebParam(name = "login") final @NotNull String login,
                               @WebParam(name = "password") final @NotNull String password);

    @WebMethod
    public AccessRole showAccessRole(@WebParam(name = "session") final @NotNull Session session);
}

