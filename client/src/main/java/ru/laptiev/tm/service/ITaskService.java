package ru.laptiev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
@WebService
public interface ITaskService {
    String PORT = "TaskServiceService";
    String NAME = "http://service.tm.laptiev.ru/";
    String WSDL = "http://0.0.0.0:8080/TaskEndPoint?wsdl";

    @WebMethod
    public Object getTaskRepository();

    @WebMethod
    public void setTaskRepository(
            @WebParam(name = "taskRepository") final @NotNull Object taskRepository);
    @WebMethod
    void newTask(
            @WebParam(name = "projectID") @NotNull String projectID,
            @WebParam(name = "taskName") @NotNull String taskName,
            @WebParam(name = "userID") @NotNull String userID
    );

    @WebMethod
    List<TaskDTO> showTasks(
            @WebParam(name = "projectID") @NotNull String projectID
    );

    @WebMethod
    void changeTask(
            @WebParam(name = "taskName") @NotNull String taskName,
            @WebParam(name = "taskNewName") @NotNull String taskNewName
    );

    @WebMethod
    void deleteTask(
            @WebParam(name = "taskName") @NotNull String taskName
    );

    @WebMethod
    void deleteByProjectID(
            @WebParam(name = "projectID") @NotNull String projectID
    );

    @WebMethod
    List<String> sortTasksByDateOfCreation(
            @WebParam(name = "userID") @NotNull final String userID
    );

//    @WebMethod
//    List<String> sortTasksByStartDate(
//            @WebParam(name = "userID") @NotNull final String userID
//    );
//
//    @WebMethod
//    List<String> sortTasksByFinishDate(
//            @WebParam(name = "userID") @NotNull final String userID
//    );

    @WebMethod
    List<String> sortTasksByStatus(
            @WebParam(name = "userID") @NotNull final String userID
    );

    @WebMethod
    List<String> globalTaskSearch(
            @WebParam(name = "text") @NotNull final String text
    );
    @WebMethod
    List<String> globalTaskSearchByDescription(
            @WebParam(name = "text") @NotNull final String text
    );

    @WebMethod
    public void simpleTaskSave();

    @WebMethod
    public void simpleTaskLoad();

    @WebMethod
    public void jacksonJSONTaskSave();

    @WebMethod
    public void jacksonJSONTaskLoad();

    @WebMethod
    public void jacksonXMLTaskSave();

    @WebMethod
    public void jacksonXMLTaskLoad();

    @WebMethod
    public void jaxBJSONTaskSave();

    @WebMethod
    public void jaxBJSONTaskLoad();

    @WebMethod
    public void jaxBXMLTaskSave();

    @WebMethod
    public void jaxBXMLTaskLoad();

}


