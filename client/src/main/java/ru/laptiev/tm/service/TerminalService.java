package ru.laptiev.tm.service;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public final class TerminalService  {
    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    public String readLine() {
        String line = scanner.nextLine();
        return line;
    }
}
