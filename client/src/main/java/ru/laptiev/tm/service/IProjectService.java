package ru.laptiev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.laptiev.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectService {
     String PORT = "ProjectServiceService";
     String NAME = "http://service.tm.laptiev.ru/";
     String WSDL = "http://0.0.0.0:8080/ProjectEndPoint?wsdl";

     @WebMethod
     public Object getProjectRepository();

     @WebMethod
     public void setProjectRepository(
             @WebParam(name = "projectRepository") final @NotNull Object projectRepository);

     @WebMethod
     void newProject(
             @WebParam(name = "projectName") @NotNull final String projectName,
             @WebParam(name = "userID") @NotNull final String userID);

     @WebMethod
     List<ProjectDTO> showProjects(
             @WebParam(name = "userID") @NotNull final String userID
     );

     @WebMethod
     void changeProject(
             @WebParam(name = "projectName") @NotNull final String projectName,
             @WebParam(name = "projectNewName") @NotNull final String projectNewName
     );

     @WebMethod
     void deleteProject(
             @WebParam(name = "projectName") @NotNull final String projectName
     );

     @WebMethod
     String findProjectID(
             @WebParam(name = "projectName") @NotNull final String projectName
     );

     @WebMethod
     List<String> sortProjectsByDateOfCreation(
             @WebParam(name = "userID") @NotNull final String userID
     );

//     @WebMethod
//     List<String> sortProjectsByStartDate(
//             @WebParam(name = "userID") @NotNull final String userID
//     );
//
//     @WebMethod
//     List<String> sortProjectsByFinishDate(
//             @WebParam(name = "userID") @NotNull final String userID
//     );

     @WebMethod
     List<String> sortProjectsByStatus(
             @WebParam(name = "userID") @NotNull final String userID
     );

     @WebMethod
      List<String> globalProjectSearch(
             @WebParam(name = "text") @NotNull final String text
     );
     @WebMethod
     List<String> globalProjectSearchByDescription(
             @WebParam(name = "text") @NotNull final String text
     );

     @WebMethod
     public void simpleProjectSave();

     @WebMethod
     public void simpleProjectLoad();

     @WebMethod
     public void jacksonJSONProjectSave();

     @WebMethod
     public void jacksonJSONProjectLoad();

     @WebMethod
     public void jacksonXMLProjectSave();

     @WebMethod
     public void jacksonXMLProjectLoad();

     @WebMethod
     public void jaxBJSONProjectSave();

     @WebMethod
     public void jaxBJSONProjectLoad();

     @WebMethod
     public void jaxBXMLProjectSave();

     @WebMethod
     public void jaxBXMLProjectLoad();
}






