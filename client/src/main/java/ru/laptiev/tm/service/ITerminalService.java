package ru.laptiev.tm.service;

public interface ITerminalService {
     String readLine();
}
