package ru.laptiev.tm.service;

import ru.laptiev.tm.commands.AbstractCommand;
import ru.laptiev.tm.entity.Session;

import java.util.List;

public interface ServiceLocator {
    TerminalService getTerminalService();

    void setCurrentSession(Session session);

    void setCurrentUserID(String userID);

    List<AbstractCommand> getCommands();

    IProjectService getIProjectService();

    ITaskService getITaskService();

    IUserService getIUserService();

    ISessionService getISessionService();

}
