package ru.laptiev.tm;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.laptiev.tm.commands.AbstractCommand;
import ru.laptiev.tm.entity.Session;
import ru.laptiev.tm.service.ServiceLocator;
import ru.laptiev.tm.service.*;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Getter
@Setter
public final class Bootstrap implements ServiceLocator {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private IProjectService iProjectService;
    private ITaskService iTaskService;
    private IUserService iUserService;
    private ISessionService iSessionService;
    private TerminalService terminalService;
    private Session currentSession;
    private String currentUserID;
    final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("DD.MM.YYYY");

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void registry(@NotNull final AbstractCommand command) throws Exception {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty()) throw new Exception();
        if (cliDescription == null || cliDescription.isEmpty()) throw new Exception();
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    private void start() throws Exception {
        terminalService = new TerminalService();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command;
        while (true) {
            System.out.print("Enter command: ");
            command = this.getTerminalService().readLine();
            if ("exit".equals(command)) {
                return;
            }
            execute(command);
        }
    }

    private void execute(@NotNull final String command) throws Exception {
        if (command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            System.out.println("Wrong command");
            return;
        }

        if ("help".equals(abstractCommand.command()) || "about".equals(abstractCommand.command()) || "simple save".equals(abstractCommand.command()) || "simple load".equals(abstractCommand.command()) || "jaxb xml save".equals(abstractCommand.command()) || "jaxb xml load".equals(abstractCommand.command()) || "jaxb json save".equals(abstractCommand.command()) || "jaxb json load".equals(abstractCommand.command()) || "jackson json save".equals(abstractCommand.command()) || "jackson json load".equals(abstractCommand.command()) || "jackson xml save".equals(abstractCommand.command()) || "jackson xml load".equals(abstractCommand.command())) {
            abstractCommand.execute();
            return;
        }
        if (currentSession == null && ("log in".equals(abstractCommand.command()) || "new user".equals(abstractCommand.command()))) {
            abstractCommand.execute();
        } else if (currentSession != null && this.getIUserService().sessionCheck(currentSession.getSignature(), currentUserID) && abstractCommand.getAccessRole().contains(iSessionService.showAccessRole(currentSession))) {
            abstractCommand.execute();
        }
    }


    public void init(@Nullable Set<Class<? extends AbstractCommand>> classes) throws Exception {
        iProjectService = Service.create(new URL(IProjectService.WSDL), new QName(IProjectService.NAME, IProjectService.PORT)).getPort(new QName(IProjectService.NAME, "ProjectServicePort"), IProjectService.class);
        iTaskService = Service.create(new URL(ITaskService.WSDL), new QName(ITaskService.NAME, ITaskService.PORT)).getPort(new QName(ITaskService.NAME, "TaskServicePort"), ITaskService.class);
        iUserService = Service.create(new URL(IUserService.WSDL), new QName(IUserService.NAME, IUserService.PORT)).getPort(new QName(IUserService.NAME, "UserServicePort"), IUserService.class);
        iSessionService = Service.create(new URL(ISessionService.WSDL), new QName(ISessionService.NAME, ISessionService.PORT)).getPort(new QName(ISessionService.NAME, "SessionServicePort"), ISessionService.class);
        for (Class<? extends AbstractCommand> command : classes) {
            registry(command.newInstance());
        }


        this.start();
    }


}
